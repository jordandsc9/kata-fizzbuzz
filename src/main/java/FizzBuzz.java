public class FizzBuzz {

    public String transformNumberToFizzEquivalent (int number){
        boolean isModuloTrois = modulo(number, 3);
        boolean isModuloCinq = modulo(number, 5);
        if (isModuloCinq & isModuloTrois){
            return"FizzBuzz";
        }
        if (isModuloTrois){
            return "Fizz";
        } else
        if(isModuloCinq){
            return "Buzz";
        }
        return Integer.toString(number);
    }   

    private boolean modulo(int number, int multiple){
        return number % multiple == 0;
    }
}
