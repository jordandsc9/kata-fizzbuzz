import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FizzBuzzTest {

    FizzBuzz fizz = new FizzBuzz();

    @Test
    public void fizzTroisTest(){
        Assertions.assertEquals("Fizz",fizz.transformNumberToFizzEquivalent(3));

    }

    @Test
    public void fizzCinqTest(){
        Assertions.assertEquals("Buzz",fizz.transformNumberToFizzEquivalent(5));
    }
}
